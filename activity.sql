/**********************************
                                 
	Activity Main Requirements   
                                 
**********************************/

SELECT customerName FROM customers WHERE country = "Philippines";

SELECT customerName FROM customers WHERE country = "USA";

SELECT contactLastName, contactFirstName 
FROM customers 
WHERE customerName = "La Rochelle Gifts";

SELECT productName,MSRP FROM products WHERE productName = "The Titanic";

SELECT firstName,lastName 
FROM employees 
WHERE email = "jfirrelli@classicmodelcars.com";

SELECT customerName FROM customers WHERE state IS NULL;

SELECT firstName,lastName,email 
FROM employees 
WHERE lastName = "Patterson" AND firstName = "Steve";

SELECT customerName,country,creditLimit 
FROM customers 
WHERE country != "USA" AND creditLimit > 3000;

SELECT COUNT(comments) FROM orders WHERE comments LIKE "%DHL%";

SELECT productLine FROM productlines WHERE textDescription LIKE "%state of the art%";

SELECT DISTINCT country FROM customers;

SELECT DISTINCT status FROM orders;

SELECT customerName,country FROM customers WHERE country IN ("USA","France","Canada");

SELECT firstName,lastName,city FROM employees
JOIN offices ON offices.officeCode = employees.officeCode
WHERE city = "Tokyo";

SELECT customerName FROM customers
JOIN employees ON employees.employeeNumber = customers.salesRepEmployeeNumber
WHERE lastName = "Thompson" AND firstName = "Leslie";

SELECT productName,quantityInStock FROM products
WHERE productLine = "planes" AND quantityInStock < 1000;

SELECT customerName FROM customers WHERE phone LIKE "+81%";

SELECT COUNT(customerNumber) FROM customers WHERE country = "UK";

/**********************************
                                 
	Stretch Goals Requirements    
                                 
**********************************/

SELECT productName, customerName FROM orderdetails
JOIN orders ON orders.orderNumber = orderdetails.orderNumber
JOIN customers ON customers.customerNumber = orders.customerNumber
JOIN products ON products.productCode = orderdetails.productCode 
WHERE customerName = "Baane Mini Imports";

SELECT employees.firstName,employees.lastName FROM employees
JOIN employees AS emp ON emp.employeeNumber = employees.reportsTo
WHERE emp.firstName = "Anthony" AND emp.lastName = "Bow";

SELECT productName,MSRP FROM products ORDER BY MSRP DESC LIMIT 1;

SELECT productlines.productLine, COUNT(*) AS productNumber FROM products
JOIN productlines ON productlines.productLine = products.productLine
GROUP BY productLine;

SELECT COUNT(status) FROM orders
WHERE status = "Cancelled";